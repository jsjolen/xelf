#+OPTIONS: *:nil
** GLIDE-WINDOW-TO (generic function)
 
: (BUFFER X Y &OPTIONAL Z)
Configure window to glide smoothly to the point X,Y.
