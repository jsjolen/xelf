#+OPTIONS: *:nil
** MOVE-WINDOW-TO (generic function)
 
: (BUFFER X Y &OPTIONAL Z)
Move the buffer's window to X,Y.
