#+OPTIONS: *:nil
** DEFUN-MEMO (macro)
 
: (NAME ARGS MEMO-ARGS &BODY BODY)
Define a memoized function named NAME.
ARGS is the lambda list giving the memoized function's arguments.
MEMO-ARGS is a list with optional keyword arguments for the
memoization process: :KEY, :VALIDATOR, and :TEST.
