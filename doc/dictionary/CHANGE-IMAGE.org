#+OPTIONS: *:nil
** CHANGE-IMAGE (generic function)
 
: (NODE IMAGE)
Set the image of NODE to be IMAGE.
