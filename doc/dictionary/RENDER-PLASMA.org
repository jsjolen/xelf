#+OPTIONS: *:nil
** RENDER-PLASMA (function)
 
: (WIDTH HEIGHT &KEY (GRAININESS 1.0) ARRAY)
Return a rectangle subdivision noise field with WIDTH,HEIGHT.
