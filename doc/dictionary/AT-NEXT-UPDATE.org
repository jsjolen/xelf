#+OPTIONS: *:nil
** AT-NEXT-UPDATE (macro)
 
: (&BODY BODY)
Run the forms in BODY at the next game loop update.
