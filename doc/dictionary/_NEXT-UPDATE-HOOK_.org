#+OPTIONS: *:nil
** *NEXT-UPDATE-HOOK* (variable)
Hook run after each update. Value is erased each time.
