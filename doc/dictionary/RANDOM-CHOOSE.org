#+OPTIONS: *:nil
** RANDOM-CHOOSE (function)
 
: (SET)
Randomly choose one element of the list SET and return it.
