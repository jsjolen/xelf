#+OPTIONS: *:nil
** STOP-FOLLOWING (generic function)
 
: (BUFFER)
Stop all buffer following.
