#+OPTIONS: *:nil
** *QUADTREE-DEPTH* (variable)
Current depth of the quadtree.
