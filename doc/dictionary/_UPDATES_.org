#+OPTIONS: *:nil
** *UPDATES* (variable)
The number of game loop updates since startup.
