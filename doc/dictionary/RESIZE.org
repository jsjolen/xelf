#+OPTIONS: *:nil
** RESIZE (generic function)
 
: (NODE WIDTH HEIGHT)
Resize the NODE to be WIDTH by HEIGHT units.
