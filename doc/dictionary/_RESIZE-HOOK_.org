#+OPTIONS: *:nil
** *RESIZE-HOOK* (variable)
Hook to be run after user resizes window.
