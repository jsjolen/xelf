#+OPTIONS: *:nil
** UPDATE (generic function)
 
: (NODE)
Update the node's game logic for one frame.
