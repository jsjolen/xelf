#+OPTIONS: *:nil
** COLLIDE (generic function)
 
: (THIS THAT)
Trigger defined collision methods for when THIS collides with THAT.
If a collision method is defined as (COLLIDE CLASS-1 CLASS-2), then
this COLLIDE will trigger when QUADTREE-COLLIDE is called on instances
of CLASS-1 that are colliding with instances of CLASS-2. 

If (COLLIDE CLASS-2 CLASS-1) is also defined, it will be triggered
only when QUADTREE-COLLIDE is called on colliding instances of
CLASS-2.

If you always want both orderings of the class pair's COLLIDE to be
triggered, then you must call QUADTREE-COLLIDE on every object in the
scene. This is done by default but can be interfered with if you use
the slot COLLISION-TYPE.
