#+OPTIONS: *:nil
** BACKWARD (generic function)
 
: (NODE DISTANCE)
Move NODE backward DISTANCE units along the aim direction.
