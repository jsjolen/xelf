#+OPTIONS: *:nil
** MOVE-WINDOW (generic function)
 
: (BUFFER DX DY &OPTIONAL DZ)
Move the buffer's window by DX,DY.
