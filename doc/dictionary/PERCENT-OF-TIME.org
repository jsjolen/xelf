#+OPTIONS: *:nil
** PERCENT-OF-TIME (macro)
 
: (PERCENT &BODY BODY)
Evaluate the BODY forms PERCENT percent of the time.
