#+OPTIONS: *:nil
** WITH-FONT (macro)
 
: (FONT &REST BODY)
Evaluate forms in BODY with FONT as the current font.
