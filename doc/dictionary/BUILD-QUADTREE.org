#+OPTIONS: *:nil
** BUILD-QUADTREE (function)
 
: (BOUNDING-BOX0 &OPTIONAL (DEPTH *DEFAULT-QUADTREE-DEPTH*))
Build a complete quadtree structure inside BOUNDING-BOX0 with DEPTH levels.
