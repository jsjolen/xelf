#+OPTIONS: *:nil
** CREATE-PATH (function)
 
: (FINDER &KEY (HEIGHT *PATH-GRID-RESOLUTION*) (WIDTH *PATH-GRID-RESOLUTION*)
   (BUFFER (CURRENT-BUFFER)))
Return a new path for the object FINDER in the buffer BUFFER.
The arguments HEIGHT and WIDTH define the dimensions of the grid of
cells to be searched for traversability.
