#+OPTIONS: *:nil
** SNAP-WINDOW-TO-NODE (generic function)
 
: (BUFFER NODE)
Snap the window to the node NODE.
