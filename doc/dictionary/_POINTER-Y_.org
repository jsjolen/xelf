#+OPTIONS: *:nil
** *POINTER-Y* (variable)
Current window-relative y-coordinate of the mouse pointer.
