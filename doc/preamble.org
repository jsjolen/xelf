#+INFOJS_OPT: view:info mouse:underline up:index.html home:http://xelf.me toc:t  ftoc:t ltoc:t

* About this document  

#+BEGIN_QUOTE  
Use the N and P keys to flip to the Next and Previous pages, or click
the links in the header. Press B to go Back or "?" for help. This
document can also be browsed as a [[file:reference-flat.html][single large web page]].
#+END_QUOTE

This is the documentation reference for the Xelf game engine.

It includes all the documentation strings extracted from the Common
Lisp source code. The rest of the documentation can be found at the
Xelf site.

 - Xelf home page: http://xelf.me/  
 - Xelf [[http://gitlab.com/dto/xelf][source code repository]] at gitlab  

Known issues:  

 - Current stable version only supports SDL 1.2 and legacy OpenGL
 - Some things are not documented yet (pathfinding, networking)
  
* Documentation strings
