#+TITLE: Porting xelf to clinch

** TODO should quadrille be base class? need new Quadrille which is subclass of clinch:node
** TODO get project-file stuff working in xelf
** TODO read up on modern opengl 3.0 and shaders, uniforms, normals, 
** TODO https://github.com/BradWBeer/cl-pango
** DONE [#A] re-merge quadrille
   CLOSED: [2016-12-18 Sun 17:14]
** TODO [#A] add 3d distance function
** TODO [#A] make-quad-for-image
** TODO [#A] keyboard handling
** TODO [#B] screen setup with make-orthogonal-transform
** TODO add quad to node, then node add to *root* node
** TODO on-text-input uses a string char
** TODO (setf (uniform entity "t1") texture)

* brad notes: 4 things in an entity
** shader program interprets attribute values
*** vertex shader called once for each triangle vertex
*** fragment shader called once for each pixel in that triangle
*** You can pass info from vertex->fragment shader but not the reverse
** string-named attributes are different for each vertex
*** points are attributes matching 1-to-1 with vertices
*** "v" vertices 
*** "n" normals
*** "tc1" texcoord1
*** "c" color
** uniforms are the same for all calls to shader
** indexes of points

