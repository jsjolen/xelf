#+TITLE: Keeping Game Designs Simple and Accessible

* Why? 
** Simple games are easier to develop, right?
** Accessible games can be played by more people
** Simple games also often have wider inherent appeal

* What does Simple mean?
** Rough working criterion of simplicity: "simpler designs have fewer parts"
*** Make bullet lists and just count the parts!
*** Prioritize list items with A, B, C
**** Category A: need it now
**** Category B: want it soon
**** Category C: would be nice someday
*** Be a ruthless overlord in postponing/deprioritizing stuff you don't really need
**** YouArentGonnaNeedIt! http://c2.com/cgi/wiki?YouArentGonnaNeedIt
**** DoTheSimplestThingThatCouldPossiblyWork! http://c2.com/cgi/wiki?DoTheSimplestThingThatCouldPossiblyWork
**** WhatIsSimplest? http://c2.com/cgi/wiki?WhatIsSimplest
*** Be flexible: priorities can change
****  Don't be too reluctant to postpone something. It won't disappear if you keep good notes.
*** Prioritize, then work only on Category A until it is empty due to finishing/deprioritizing tasks.
*** Then, re-prioritize and repeat.

* Game design elements that tend to introduce complication and multiply workload
*** Elaborate weapon/magic systems with many choices
**** Say you have six weapons, so justifying their existence must be gameplay situations / enemies that are more susceptible to each weapon
**** If you had 12 weapons, that'd be twice the work to make the new objects relevant
*** Long lists of character classes/types
**** If you have just a few, like Gauntlet (Thief/Magician/Warrior) then it's still simple --- although still more complex than just having one.
**** If you have dozens, you have to have many (probably dozens) of interacting factors in the game design in order to justify distinguishing the character classes
**** Do Mage and Necromancer have totally different magic systems, and can't equip each other's weapons or armor? Even more multiplication, and many objects which are explicitly prevented from working together.
*** Exploration as a mechanic
**** You have to have more places to go, and reasons to go there
*** Crafting items by combining other items
**** Needing to carry and remember all that stuff
**** Can lead to large numbers of combinations (or just tons that don't produce anything of value, which isn't good either)
*** Alliance/guild/reputation systems
**** If your guild membership affects many events in the game, you have to build and test all those alternate paths.
**** If it DOESN'T affect many events, then why bother with it?

* What does Accessible mean?
** "Can the player pick up the gamepad/keyboard/tablet and understand the basics of gameplay in 60 seconds?"
** Measurement of accessibility: interface between player and game is simple and obvious (or ALMOST obvious) 
*** Surface area: how many buttons does the user need to learn/remember?
*** Are there a lot of screens or menus, or does the action mostly focus on the player-character?
*** Player can see/hear/reason about what is going on directly, not by parsing onscreen tables of item stats.
** A simple game design is already well on its way to being accessible too
** Adjustable difficulty
*** allow a more general audience to participate
*** multiplayer games can allow different difficulty settings per player so that different skill levels can play together
** PC keyboard limitations: you can only rely on arrows+shift being simultaneously registered as pressed
*** Design for arrows/shift/spacebar/gamepad+2buttons
** Users with special needs
*** Much info at http://gameaccessibilityguidelines.com/

* Game design elements that aren't very accessible
** Complicated user interfaces with many screens
** Text-heavy games (I learned this the hard way!)
** Requiring user distinguish many colors
** Consistent story/world setting
** Requiring a mouse
** Multi-level menus
** Presenting vital information through color or sound alone

* Learning from Atari 2600/7800 designs
** Undeniably simple controls---8 directions and one fire button.
** Limited RAM/CPU led to simpler, more focused designs
** Game Reset
*** easy testing for developer, less frustration for player when game ends suddenly. 
*** easy feedback loop to help the player "get" the new game more quickly.. die a few times etc
** Game Variation Select
*** variety is the spice of life 
*** co-op and/or versus play
** Difficulty adjustment switches
** BW/Color switch 
*** Metaphor for accessibility to everyone
*** Color vision impairment 

* How to use these ideas in writing and outlining 
** Write paragraph or two of prose about your project. 
** Break down prose description into TODO items
** Avoid long, open-ended descriptions, or laundry-lists of what might be cool someday.
*** Stick with lists of what you can do in the near-to-medium-term.
** Outlining tools 
