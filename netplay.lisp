(in-package :xelf)

;;; Utility functions

(defun parse-ip (string)
  (block parsing
    (let (s ip)
      (setf s string)
      (let ((pos (position #\. s)))
	(if (null pos)
	    (return-from parsing)
	    (push (subseq s 0 pos) ip))
	(setf s (subseq s (1+ pos)))
	(setf pos (position #\. s))
	;;
	(if (null pos)
	    (return-from parsing)
	    (push (subseq s 0 pos) ip))
	(setf s (subseq s (1+ pos)))
	(setf pos (position #\. s))
	;;
	(if (null pos)
	    (return-from parsing)
	    (push (subseq s 0 pos) ip))
	(setf s (subseq s (1+ pos)))
	;;
	(push s ip))
      (mapcar #'(lambda (x)
		  (parse-integer x :junk-allowed t))
	      (nreverse ip)))))

(defun reformat-ip (ip)
  (destructuring-bind (a b c d) ip
    (concatenate 'string
		 (prin1-to-string a)
		 "."
		 (prin1-to-string b)
		 "."
		 (prin1-to-string c)
		 "."
		 (prin1-to-string d))))

;;; Preventing duplicate sounds

(defvar *current-sounds* nil)

(defun initialize-sounds ()
  (setf *current-sounds* nil))

(defun clear-sounds ()
  (initialize-sounds))

(defun sample-playing-p (sample)
  (find sample *current-sounds* :test 'equal))

(defun play-sample* (sample)
  (when (null *current-sounds*)
    (initialize-sounds))
  (unless (sample-playing-p sample)
    (pushnew sample *current-sounds* :test 'equal)
    (play-sample sample)))

;;; Global parameters

(defvar *inhibit-splash-screen* nil)

(defun find-heading-direction (float)
  (or (heading-direction float) :left))

(defun quantize-heading (h) 
  (direction-heading (find-heading-direction h)))

;;; Utility functions

(defun holding-down-arrow-p () (or (keyboard-down-p :kp2) (keyboard-down-p :down)))
(defun holding-up-arrow-p () (or (keyboard-down-p :kp8) (keyboard-down-p :up)))
(defun holding-left-arrow-p () (or (keyboard-down-p :kp4) (keyboard-down-p :left)))
(defun holding-right-arrow-p () (or (keyboard-down-p :kp6) (keyboard-down-p :right)))
(defun holding-shift-p () (xelf::holding-shift))
(defun holding-enter-p () (or (keyboard-down-p :kp-enter)
			     (keyboard-down-p :return)))
(defun holding-return-p () (holding-enter))

(defun arrow-keys-direction ()
  (cond 
    ((and (holding-down-arrow-p) (holding-right-arrow-p)) :downright)
    ((and (holding-down-arrow-p) (holding-left-arrow-p)) :downleft)
    ((and (holding-up-arrow-p) (holding-right-arrow-p)) :upright)
    ((and (holding-up-arrow-p) (holding-left-arrow-p)) :upleft)
    ((holding-down-arrow-p) :down)
    ((holding-up-arrow-p) :up)
    ((holding-left-arrow-p) :left)
    ((holding-right-arrow-p) :right)))

(defun holding-button-p (joystick)
  (when (numberp joystick)
    (some #'(lambda (button) (joystick-button-pressed-p button joystick))
	  '(0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20))))

(defun derange (things)
  (let ((len (length things))
	(things2 (coerce things 'vector)))
    (dotimes (n len)
      (rotatef (aref things2 n)
	       (aref things2 (random len))))
    (coerce things2 'list)))

(defun find-instances (buffer class-name)
  (when (typep buffer (find-class 'buffer))
    (let ((objects (objects buffer)))
      (when objects
	(loop for thing being the hash-keys in objects
	      when (typep (find-object thing t) (find-class class-name))
		collect (find-object thing t))))))

;;; Continuous xelf object streaming 

(defvar *node-stream* nil)
(defparameter *node-stream-rate* 2)

(defun node-stream () *node-stream*)
(defun set-node-stream (val) (setf *node-stream* val))
(defsetf node-stream set-node-stream)
(defun next-node () (pop (node-stream)))
(defun streaming () (not (null (node-stream))))

(defun make-node-stream (&optional (buffer (current-buffer)))
  (mapc #'(lambda (x) (slot-value x 'xelf::uuid))
	(get-nodes buffer)))

(defun begin-node-stream (&optional (stream (make-node-stream)))
  (setf (node-stream) stream))

(defun send-node (node) 
  (send-message (current-buffer) (cons '+state+ (list (list '*stub* (flatten (find-object node)))))))

(defun send-node-stream (&optional (rate *node-stream-rate*))
  (dotimes (n rate) 
    (when (streaming)
      (send-node (next-node)))))

(defvar *census* nil)

(defmethod make-census ((buffer buffer))
  (let ((uuids (make-hash-table :test 'equal :size 64)))
    (do-nodes (node buffer)
      (setf (gethash (slot-value node 'xelf::uuid) uuids)
	    (slot-value node 'xelf::uuid)))
    (setf *census* uuids)
    ;; (verbosely "Created census with ~S uuids." (hash-table-count uuids))
    uuids))

(defmethod clean-buffer ((buffer buffer))
  (let ((census *census*)
	(count 0))
    (when census
      (progn (do-nodes (node buffer)
	       (when (not (gethash (slot-value node 'xelf::uuid) 
				   census))
		 (remove-node buffer node)
		 (incf count)))
	     (setf *census* nil)))))
      ;; (verbosely "Cleaned local buffer (removing ~S uuids) via census with ~S uuids."
      ;; 		 count (hash-table-count census)))))

;;; Handshaking to establish interactivity

(defvar *flag-received-p* nil)
(defvar *flag-sent-p* nil)
(defun flag-received-p () *flag-received-p*)
(defun flag-sent-p () *flag-sent-p*)

(defun host-id-strings ()
  (list (machine-instance) " "
	(software-type) " "
	(software-version) " "
	(machine-type) " "
	(machine-version)))

(defun host-id ()
 (apply #'concatenate 'string (host-id-strings)))

(defun make-flag () 
  (list +flag+ (host-id) *received-messages-count*))

;;; Keeping track of time

(defun current-time () *updates*)

(defun with-timestamp (&rest args)
  (list args (current-time)))

(defvar *last-message-timestamp* 0)

(defun expired-time-p (time)
  (< (+ 1 time) ;; allow off-by-one in case server clock update arrives
	       ;; out of order
     (current-time)))

(defun last-message-time ()
  *last-message-timestamp*)

;;; Graceful data reduction 

(defvar *degrade-stream-p* nil)
(defun degrade-stream-p () *degrade-stream-p*)

(defun degrade-time-p (time)
  (when *degrade-stream-p*
    (evenp time)))

(defmacro gracefully (&body body)
  `(unless (degrade-time-p (current-time))
     ,@body))

(defmethod try-degrade-stream ((buffer buffer))
  (if (not *degrade-stream-p*)
      (setf *degrade-stream-p* t)
      (time-out)))

(defmethod try-degrade-stream :before ((buffer buffer))
  (if *degrade-stream-p*
      (logging "Cannot degrade stream further. Packet loss too high.")
      (logging "Degrading streams to 30 updates/second.")))

(defmethod try-improve-stream ((buffer buffer)) nil)

;;; Connection timeout

(defvar *echo-timer* 0)
(defparameter *echo-timeout-seconds* 30)
(defparameter *echo-timeout-length* (seconds *echo-timeout-seconds*))

(defun renew-echo-timer ()
  (setf *echo-timer* *echo-timeout-length*))

(defun log-horizontal-rule ()
  (logging "--------------------------------------------------------------- "))

(defun time-out ()
  (logging "Connection timed out after ~S seconds." *echo-timeout-seconds*)
  (close-netplay)
  (setf *flag-sent-p* nil)
  (setf *flag-received-p* nil))

(defun update-echo-timer ()
  (when (plusp *echo-timer*)
    (decf *echo-timer*)
    ;; did we just time out?
    (when (zerop *echo-timer*)
      (time-out)
      (when (current-buffer)
	(process-time-out (current-buffer))))))

(defun update-burst-timer ()
  (when (plusp *burst-timer*)
    (decf *burst-timer*)
    ;; did we just finish burst?
    (when (zerop *burst-timer*)
      (verbosely "Finished burst mode ~A" 
	       (if (connectedp (current-buffer))
		   "successfully."
		   "without connection.")))))

(defun timed-out-p ()
  (not (plusp *echo-timer*)))

;;; Doing things periodically 

(defparameter *chatter-period* (seconds 15))
(defun chatter-period () *chatter-period*)

(defmacro periodically (&body body)
  `(when (and (not (timed-out-p))
	      (zerop (mod (current-time) (chatter-period))))
     ,@body))

(defparameter *query-period* (seconds 1))
(defun query-period () *query-period*)

(defmacro querying (&body body)
  `(when (and (not (timed-out-p))
	      (zerop (mod (current-time) (query-period))))
     ,@body))

(defparameter *burst-period* 5)
(defparameter *burst-duration* 33)
(defparameter *burst-timer* 0)

(defun burst-mode ()
  (verbosely "Entering burst mode.")
  (setf *burst-timer* *burst-duration*))

(defun burst-mode-p ()
  (plusp *burst-timer*))

(defun burst-period () *burst-period*)

(defmacro bursting (&body body)
  `(when (and 
	  (not (timed-out-p))
	  (burst-mode-p)
	  (zerop (mod (current-time) (burst-period))))
     ,@body))

(defmacro always-bursting (&body body)
  `(when (and 
	  (not (timed-out-p))
	  (zerop (mod (current-time) (burst-period))))
     ,@body))

;;; Are we playing client, server, or NIL?

(defparameter *netplay* nil)

;;; Terminal display 

(defvar *terminal-lines* nil)
(defvar *terminal-timer* 1)
(defvar *terminal-show-time* (seconds 15))
(defvar *terminal-bottom* (- *screen-height* (units 1)))
(defvar *terminal-left* (units 1))

(defun show-terminal (&optional (time *terminal-show-time*))
  (setf *terminal-timer* time))

(defun hide-terminal ()
  (setf *terminal-timer* 0))

(defun update-terminal-timer ()
  (when (plusp *terminal-timer*)
    (decf *terminal-timer*)))

(defparameter *terminal-display-lines* 20)
(defparameter *terminal-font* "sans-mono-bold-12")

(defparameter *lines-per-screen* 38)

(defun lines-per-screen () *lines-per-screen*)

(defun clear-terminal () (setf *terminal-lines* nil))
(defun last-terminal-line () (first *terminal-lines*))

(defun add-terminal-line (string) 
  (when (and string (not (string= string (last-terminal-line))))
    (push string *terminal-lines*)))

(defun format-terminal (format-string &rest args)
  (add-terminal-line (clean-string (apply #'format nil format-string args))))

(defparameter *terminal-foreground-color* "white")
(defparameter *terminal-error-color* "red")
(defparameter *terminal-background-color* "gray30")
(defparameter *terminal-bottom* (- *screen-height* (units 2)))

(defun nice-string (string)
  (if (zerop (length string))
      " "
      string))

(defun draw-terminal (&optional (number-of-lines *terminal-display-lines*) translucent)
  (let* ((x *terminal-left*)
	 (y *terminal-bottom*)
	 (lines *terminal-lines*)
	 (count (min (abs number-of-lines) (length *terminal-lines*)))
	 (line-height (font-height *terminal-font*))
	 (height (* count line-height))
	 (n 0))
    (when lines
      (loop while (and lines (< n count)) do
	(draw-string (nice-string (pop lines)) x (- y line-height)
		     :color *terminal-foreground-color*
		     :font *terminal-font*)
	(decf y line-height)
	(incf n))
      (let ((last-line (first *terminal-lines*)))
	(when (and last-line
		   (not *prompt*))
	  (draw-string (nice-string last-line) *terminal-left* (- *terminal-bottom* line-height)
		       :color (random-choose '("cyan" "white" "yellow"))
		       :font *terminal-font*))))))

(defun draw-terminal-maybe (&optional number-of-lines translucent)
  (when (plusp *terminal-timer*)
    (draw-terminal number-of-lines translucent)))

;;; Logging messages to screen and/or standard output

(defun role-indicator ()
  (case *netplay*
    (:client "- Client")
    (:server "+ Server")
    (otherwise " ")))
  
(defun local-machine-description ()
  (if *netplay* (machine-instance) " "))

(defun tee (string &rest args)
  (apply #'message string args))
  ;;(apply #'format-terminal string args))

(defun logging (format &rest args)
  (apply #'tee
	 (concatenate 'string 
		      (if *netplay* "~A ~A: [~S] " " ")
		      (clean-string format))
	 (append 
	  (when *netplay* (list (role-indicator)
				(short-string (local-machine-description))
				(current-time)))
	  args)))

(defparameter *verbose-p* nil)

(defun verbosely (format &rest args)
  (when *verbose-p* (apply #'logging format args)))

;;; Configuration

(defvar *output* nil)
(defvar *input* nil)
(defvar *everything* nil)
(defvar *client* nil)
(defvar *server* nil)
(defvar *remote-host* nil)
(defvar *remote-port* nil)
(defparameter *local-host* nil)
(defparameter *server-host* "0.0.0.0")
(defparameter *client-host* nil)
(defparameter *local-port* 50191)
(defparameter *base-port* 50191)
(defparameter *server-port* 50191)
(defparameter *client-port* 50191)
(defparameter *datagram-length* 4096)
(defparameter *datagram* 
  (make-array *datagram-length* :element-type '(unsigned-byte 8)))

;;; Identifying UDP messages

(defvar *id-string* "")
(defconstant +flag+ '+flag+)
(defconstant +state+ '+state+)
(defconstant +full-state+ '+full-state+)
(defconstant +input+ '+input+)
(defconstant +hash+ '+hash+)
(defconstant +xelf+ '+xelf+)
(defconstant +uuid+ '+uuid+)

(defmethod input-state-p (sexp) (eq +input+ (first sexp)))
(defmethod frame-state-p (sexp) (eq +state+ (first sexp)))
(defmethod full-state-p (sexp) (eq +full-state+ (first sexp)))
(defmethod state-p (sexp) (or (full-state-p sexp) (frame-state-p sexp)))
(defmethod flag-p (sexp) (eq +flag+ (first sexp)))
(defmethod xelf-datagram-p (sexp) (eq +xelf+ (first sexp)))

;;; Game state serialization and compression

(defun slot-definition-name (slot)
  #+ccl (ccl:slot-definition-name slot)
  #+sbcl (sb-mop:slot-definition-name slot))

(defun class-slots (class)
  #+ccl (ccl:class-slots class)
  #+sbcl (sb-mop:class-slots class))

(defun find-slots (object)
  (mapcar #'slot-definition-name (class-slots (class-of object))))

(defmethod flatten ((thing xelf::quadrille))
  (let ((class-name (class-name (class-of thing)))
	(uuid (slot-value thing 'xelf::uuid))
	(plist nil))
    (assert (and class-name (stringp uuid)))
    ;; just flatten
    (labels ((collect (field value)
	       (push (flatten value) plist)
	       (push field plist)))
	;; make flattened/cleaned plist 
      (dolist (slot (find-slots thing))
	(when (and (not (ignored-slot-p slot))
		   (slot-boundp thing slot))
	  (collect slot (slot-value thing slot))))
      ;; cons up the final flattened sexp
      (list +xelf+
	    :class class-name
	    :slots plist))))

(defun excluded-class-p (thing)
  (block excluding
    (dolist (type (mapcar #'find-class '(menu shell menubar tree entry modeline)))
      (when (typep thing type)
	(return-from excluding t)))))

(defmethod flatten :before ((buffer buffer))
  (clear-halos buffer)
  (setf (object-bag buffer)
	(remove-if #'excluded-class-p (get-nodes buffer))))
	  
(defmethod flatten (sexp)
  (cond ((consp sexp)
	 (mapcar #'flatten sexp))
	((hash-table-p sexp)
	 (labels ((hash-to-list (hash)
		    (let (plist)
		      (labels ((collect (k v)
				 (push (flatten v) plist)
				 (push k plist)))
			(maphash #'collect hash)
			(cons +hash+ (cons (hash-table-test hash) plist))))))
	   (hash-to-list sexp)))
	(t sexp)))

(defun unflatten-hash (data test)
    (let ((plist data)
	  (hash (make-hash-table :test test)))
      (prog1 hash
	(loop while plist do
	  (let* ((key (pop plist))
		 (value (pop plist)))
	    (setf (gethash key hash) (unflatten value)))))))

(defparameter *ignored-slots* '(xelf::quadtree xelf::quadtree-node
  xelf::caption xelf::halo xelf::needs-layout xelf::visible
  xelf::pinned xelf::name xelf::mode xelf::blend xelf::tasks
  xelf::depth xelf::default-events xelf::events xelf::tags
  xelf::opacity xelf::z xelf::collision-type xelf::image-heading))

(defun ignored-slot-p (slot)
  (member slot *ignored-slots*))

(defmethod revive ((node node) revive-p) 
  ;; (unless (find-object *buffer* :noerror)_
    (multiple-value-bind (x y) (location node)
      (xelf::register-uuid node)
      (add-node (current-buffer) node revive-p)
      (move-to node x y)))

(defmethod unflatten-xelf (sexp)
  (let (revive-p)
    (destructuring-bind (&key class uuid slots) sexp
      ;; clean replacement of existing object with this UUID
      (when (find-object uuid :no-error)
	(setf revive-p t))
	;;(remove-node (current-buffer) (find-object uuid)))
      ;; create new object and replace slots that came from network
      (let ((old-node (find-object uuid :no-error)))
	(let ((node (make-instance (find-class class) :uuid uuid)))

	  (loop while slots
		do (setf (slot-value node (pop slots))
			 (pop slots)))
	  (revive node revive-p)
	  ;;(bridge-revive node (find-object uuid :noerror))
	  node)))))

(defmethod bridge-revive (a b) nil)

(defmethod unflatten (sexp)
  (if (and (consp sexp) 
	   (eq +xelf+ (first sexp)))
      (unflatten-xelf (rest sexp))
      (if (and (consp sexp)
	       (eq +hash+ (first sexp)))
	  (unflatten-hash (rest (rest sexp)) (second sexp))
	  sexp)))

(defun serialize (sexp)
  (let ((*package* (find-package :keyword))
	(*read-default-float-format* 'double-float))
    (with-standard-io-syntax 
      (prin1-to-string sexp))))

(defun unserialize (string)
  (let ((*package* (find-package :keyword))
	(*read-default-float-format* 'double-float))
    (with-standard-io-syntax
      (let ((*read-eval* nil))
	(read-from-string string)))))

(defparameter *game-variables* nil)

(defparameter *object-variables* nil)
(defvar *stub* nil)

(defparameter *other-variables* '(*stub* *census*))

(defparameter *safe-variables* nil)

(defun flatten-variable (sym)
  (list sym
	(let ((val (symbol-value sym)))
	  (if (xelfp val)
	      (flatten (find-object val))
	      (flatten val)))))

(defun unflatten-variable (data)
  (destructuring-bind (var value) data
    ;; don't allow updating random stuff
    (if (not (member var *safe-variables*))
	(prog1 nil (logging "Warning: skipped attempt to unflatten possibly unsafe variable ~A." var))
	(let ((thing (unflatten value))
	      (old-thing (symbol-value var)))
	  (when (and (xelfp thing) 
		     (xelfp old-thing))
	    (when (not (equal (slot-value thing 'xelf::uuid)
			      (slot-value old-thing 'xelf::uuid)))
	      ;; game object is being replaced 
	      (unless
		  ;; but not for streaming dummy var!
		  (eq var '*stub*)
		(remove-node (current-buffer) old-thing))))
	  (set var thing)))))

(defun flatten-game-world ()
  (mapcar #'flatten (get-nodes (current-buffer))))

(defun flatten-game-variables (&optional force)
  (mapcar #'flatten-variable *game-variables*))

(defun flatten-object-variables (&optional force)
  (mapcar #'flatten-variable *object-variables*))

(defun unflatten-state (sexp)
  (dolist (entry (rest sexp))
    (unflatten-variable entry)))

;;; Compressing Lisp objects into timestamped datagrams

(defun compress-string (string)
  (salza2:compress-data
   (babel:string-to-octets string :encoding :utf-8)
   'salza2:zlib-compressor))

(defun compress-sexp (sexp)
  (compress-string (serialize (with-timestamp sexp))))
    
;;; Uncompressing datagrams

(defun uncompress-sexp (data &optional length)
  (let ((datagram (unserialize 
		   (babel:octets-to-string 
		    (chipz:decompress nil 'chipz:zlib data :input-end length)))))
    (values-list datagram)))
	
;;; Looking up the proper network parameters

(defmethod remote-host ((buffer buffer)) *remote-host*)

(defmethod register-remote-host ((buffer buffer) host) 
  (setf *remote-host* host))

(defmethod register-remote-host :after ((buffer buffer) host)
  (logging "Registered remote host ~A" (remote-host buffer)))

(defmethod remote-port ((buffer buffer)) *remote-port*)

(defmethod register-remote-port ((buffer buffer) port) 
  (setf *remote-port* port))

(defmethod register-remote-port :after ((buffer buffer) port)
  (logging "Registered remote port ~A" (remote-port buffer)))

;;; Local socket parameters

(defmethod local-host ((buffer buffer)) *local-host*)

(defmethod register-local-host ((buffer buffer) host) 
  (setf *local-host* host))

(defmethod register-local-host :after ((buffer buffer) host)
  (logging "Registered local host ~A" (local-host buffer)))

(defmethod local-port ((buffer buffer)) *local-port*)

(defmethod register-local-port ((buffer buffer) port) 
  (setf *local-port* port))

(defmethod register-local-port :after ((buffer buffer) port)
  (logging "Registered local port ~A" (local-port buffer)))

;;; Message-passing frontend

(defmethod send-message ((buffer buffer) sexp)
  (handler-case 
      (let ((data (compress-sexp sexp)))
	(usocket:socket-send (output-socket buffer) data (length data) 
			     :port (output-port buffer)
			     :host (output-host buffer)))
    (condition (c)
      (logging " TX ERROR: ~S" c))))

(defmethod message-ready-p ((buffer buffer))
  (when (input-socket buffer)
    (usocket:wait-for-input (input-socket buffer) :timeout 0 :ready-only t)))

(defmethod validate-message ((buffer buffer) octets length host port) 
  t)

(defmethod validate-message :around ((buffer buffer) octets length host port)
  (when (null (call-next-method))
    (verbosely "Cannot validate message.")))

(defmethod receive-message ((buffer buffer))
  (handler-case 
      (when (message-ready-p buffer) 
	(multiple-value-bind (octets length host port)
	    (usocket:socket-receive (input-socket buffer) *datagram* *datagram-length*)
	  (validate-message buffer octets length host port)
	  (multiple-value-bind (message timestamp)
	      (uncompress-sexp octets length)
	    (when (null (remote-host buffer))
	      (register-remote-host buffer host))
	    (when (null (remote-port buffer))
	      (register-remote-port buffer port))
	    (setf *last-message-timestamp*
		  (max timestamp *last-message-timestamp*))
	    (values message timestamp))))
    (condition (c)
      (logging " RX ERROR: ~S" c)
      nil)))
    
(defmethod splash-screen-maybe ()
  (unless *inhibit-splash-screen*
    (unless (and (current-buffer) (not (connectedp (current-buffer))))
      (show-terminal)
      (setf *inhibit-splash-screen* nil))))

;;; Client buffer class

(defclass client-buffer (buffer) ())

(defmethod initialize-instance :after ((buffer client-buffer) &key)
  (splash-screen-maybe)
  (log-horizontal-rule)
  (when (upnp-p) (initialize-upnp))
  (logging "PRESS [ENTER] KEY TO JOIN SERVER ~A."
	   (or *server-host* (remote-host buffer))))

(defmethod spacebar :after ((buffer client-buffer))
  (when (null *server*)
    (when (timed-out-p)
      (try-connect buffer))))

(defmethod populate ((buffer client-buffer)) nil)

(defgeneric clientp (buffer)
  (:documentation "Return non-nil if this is the Netplay client."))

(defmethod clientp ((buffer buffer)) nil)

(defgeneric serverp (buffer)
  (:documentation "Return non-nil if this is the Netplay server."))

(defmethod serverp ((buffer buffer)) nil)

(defmethod clientp ((buffer client-buffer)) t)
(defmethod serverp ((buffer client-buffer)) nil)

(defmethod output-socket ((buffer buffer)) *server*)
(defmethod input-socket ((buffer client-buffer)) *server*)

(defmethod output-port ((buffer client-buffer))
  (or (remote-port buffer) *server-port*))

(defmethod output-host ((buffer client-buffer))
  (or (remote-host buffer) *server-host*))

(defun control-character-p (character)
  (let ((code (char-code character)))
    (or (< code 32) 
	(= code 127))))

(defun remove-control-characters (string)
  (remove-if #'control-character-p (coerce string 'vector)))

(defun truncate-string (string)
  (subseq string 0 (min 118 (length string))))

(defun clean-string (string)
  (truncate-string (remove-control-characters string)))

(defun short-string (string)
  (subseq string 0 (min 12 (length string))))

(defmethod process-flag ((buffer buffer) sexp)
  (renew-echo-timer)
  (setf *flag-received-p* t)
  (setf *id-string* (clean-string (or (first (rest sexp)) "no id"))))

;; (defmethod process-flag :after ((buffer buffer) sexp) 
;;   (verbosely "Received flag with ID string ~S" (second sexp)))

(defmethod send-flag ((buffer buffer)) 
  (send-message buffer (make-flag))
  (setf *flag-sent-p* t))

;; (defmethod send-flag :after ((buffer buffer)) 
;;   (verbosely "Sent flag with ID string ~S" (host-id)))

(defmethod try-connect ((buffer client-buffer))
  (logging "Sending connection request to ~A port ~A" 
	   *server-host* *server-port*)
  (logging "Requesting client socket...")
  (open-client-socket)
  (logging "Requesting client socket... Done.")
  (register-socket buffer *server*)
  (logging "Successfully created socket ~A " (output-host buffer))
  (send-flag buffer)
  (send-flag buffer)
  (renew-echo-timer))

(defmethod connectedp :around ((buffer buffer))
  (if (and (not (clientp buffer))
	   (not (serverp buffer)))
      t
      (call-next-method)))

(defmethod connectedp ((buffer buffer))
  (and (flag-received-p)
       (not (timed-out-p))))

(defmethod connectingp ((buffer buffer))
  (and (not (flag-received-p))
       (not (timed-out-p))))

(defmethod process-input ((buffer buffer) sexp) nil)
(defmethod process-state ((buffer buffer) sexp) nil)

(defmethod process-sexp ((buffer buffer) raw-sexp &optional time)
  (let ((sexp (first raw-sexp)))
    (assert (not (null sexp)))
    (cond ((flag-p sexp) 
	   (process-flag buffer sexp))
	  ((xelf-datagram-p sexp) 
	   (process-xelf-object buffer sexp))
	  ((state-p sexp) 
	   (process-state buffer sexp))
	  ((input-state-p sexp) 
	   (process-input buffer sexp))
	  (t (error "Unknown S-expression packet: ~S" sexp)))))

(defmethod process-sexp :after ((buffer client-buffer) raw-sexp &optional time)
  (setf *flag-received-p* t))
  
(defparameter *process-all-messages* t)

(defmethod process-messages ((buffer buffer))
  (with-buffer buffer
    (loop while (message-ready-p buffer) do
      (multiple-value-bind (sexp time) (receive-message buffer)
	(when sexp 
	  (when (or *process-all-messages*
		    (not (expired-time-p time)))
	    (process-sexp buffer sexp time)))))))

(defmethod process-xelf-object ((buffer buffer) sexp)
  (unflatten (rest sexp)))
  
(defmethod process-state ((buffer client-buffer) sexp)
  (unflatten-state sexp))

(defmethod try-handshake ((buffer buffer))
  (send-flag buffer))

(defmethod register-connection ((buffer buffer))
  (hide-terminal)
  (logging "Connected to server ~A" (remote-host buffer)))

;;; Client buffer 

(defmethod send-input ((buffer buffer))
  (let ((update (make-input-update buffer)))
    (when update
      (send-message buffer update))))

(defmethod find-local-inputs ((buffer buffer))
  (delete nil (mapcar #'find-input (get-nodes buffer))))

(defmethod make-input-update ((buffer buffer))
  (let ((inputs (find-local-inputs buffer)))
    (when inputs
      (cons +input+ inputs))))

(defmethod send-input ((buffer buffer))
  (let ((update (make-input-update buffer)))
    (when update (send-message buffer update))))

(defmethod update :around ((buffer client-buffer))
  (when (upnp-p) (update-upnp))
  (update-echo-timer)
  (update-burst-timer)
  (cond ((connectingp buffer)
	 (bursting (try-handshake buffer))
	 (bursting (send-input buffer))
	 (querying (try-handshake buffer))
	 (when (message-ready-p buffer)
	   (process-messages buffer))
	 (when (connectedp buffer)
	   ;; we just got the server's response.
	   (burst-mode)
	   (register-connection buffer)))
	((connectedp buffer)
	 (querying (try-handshake buffer))
	 (call-next-method)
	 (when (message-ready-p buffer)
	   (process-messages buffer))
	 (when (connectedp buffer)
	   (clean-buffer buffer)
	   (send-input buffer)))))

;;; Server updates frog control values from the network

(defmethod update-input-state ((node node) plist time)
  (declare (ignore plist))
  (setf (input-p node) t)
  (setf (input-time node) time)
  (setf (input-update-p node) t))

;; (defmethod update-input-state :around ((node node) plist time)
;;   (if (and (numberp time) 
;; 	   (numberp (input-time node)))
;;       (when (> time (input-time node))
;; 	(call-next-method))
;;       (call-next-method)))

(defmethod clear-input-state ((node node))
  (setf (input-update-p node) nil))

(defvar *server-sent-messages-count* 0)
(defvar *sent-messages-count* 0)
(defvar *received-messages-count* 0)
(defvar *client-messages-count* 0)
(defvar *buffer-dirty-p* nil)

;;; Client buffer connection statistics gathering 

(defmethod receive-message :after ((buffer buffer))
  (incf *received-messages-count*))

;;; Server counts sent messages

(defclass server-buffer (buffer) ())

(defmethod send-message :after ((buffer buffer) sexp)
  (incf *sent-messages-count*))

(defmethod send-message :after ((buffer server-buffer) sexp)
  (incf *server-sent-messages-count*))

;;; Server gathers stats and estimates packet loss rate

(defvar *message-loss-rate* 0)

(defun message-loss-rate () *message-loss-rate*)

(defmethod input-socket ((buffer server-buffer)) *server*)

(defmethod output-port ((buffer server-buffer))
  (or (remote-port buffer) *server-port*))

(defmethod output-host ((buffer server-buffer))
  (or (remote-host buffer) *client-host*))

(defmethod clientp ((buffer server-buffer)) nil)
(defmethod serverp ((buffer server-buffer)) t)

(defmethod process-input ((buffer server-buffer) sexp)
  (dolist (input (rest sexp))
    (destructuring-bind (&key player-id time &allow-other-keys) input
      (let ((frog (find-player buffer player-id)))
	(update-input-state frog input time)))))

(defmethod initialize-instance :after ((buffer server-buffer) &key)
  (when (upnp-p) (initialize-upnp))
  (splash-screen-maybe)
  (setf *terminal-background-color* "dark olive green")
  (if (connectedp buffer)
      (setf *buffer-dirty-p* t)
      (logging "PRESS [ENTER] KEY TO START SERVER ~A ON PORT ~A..."
	       *server-host* *server-port*)))

(defmethod spacebar ((buffer server-buffer))
  (when (null *server*) 
    (when *netplay* (start-netplay *netplay*))))

(defmethod process-time-out ((buffer buffer)) nil)
  
(defmethod process-time-out ((buffer client-buffer))
  (logging "Connection to server timed out. Press ENTER to try again."))

(defmethod process-time-out ((buffer server-buffer))
  (logging "Client connection timed out. Resetting server."))

(defmethod register-connection ((buffer server-buffer))
  (logging "Connection from client ~A" (remote-host buffer))
  (burst-mode)
  (send-flag buffer)
  (send-flag buffer))

(defun estimate-message-loss-rate (sent received)
  (let* ((count (- sent received))
	 (ratio (/ received sent))
	 (rate (cfloat (/ ratio 100))))
    (verbosely 
     "Estimating ~S datagrams (about ~A percent) lost during this session."
     count rate)
    (setf *message-loss-rate* rate)))

(defmethod process-flag :after ((buffer server-buffer) sexp)
  (when (connectingp buffer)
    (register-connection buffer)))

(defmethod ambient-stream ((buffer server-buffer)) nil)

(defmethod background-stream ((buffer buffer)) nil) 

(defmethod clean-database ((buffer server-buffer))
  (let ((db xelf::*database*)
	(buf buffer))
    ;; remove from database ones that aren't in the buffer
    (do-nodes (node buf)
      (maphash #'(lambda (uuid thing)
		   (when (not (contains-node-p buf thing))
		     (with-slots (xelf::quadtree-node) thing
		       (when quadtree-node 
			 (quadtree-delete node quadtree-node)))
		     (remhash uuid db)))
	       db))))

(defmethod draw :around ((buffer buffer))
  (when (not (connectedp buffer))
    (draw-box -400 -400 (+ 500 *nominal-screen-width*) (+ 500 *nominal-screen-height*) :color *terminal-background-color*))
  (when (or (connectingp buffer)
	    (connectedp buffer))
    (call-next-method)
    (draw-terminal-maybe 
     (if (or (connectingp buffer)
	     (solop buffer))
	 (- (lines-per-screen) 10)
	 (if (connectedp buffer)
	     3
	     (lines-per-screen))) t))
  (when (not (connectedp buffer))
    (draw-terminal (lines-per-screen) t)))

(defmethod solop ((buffer buffer))
  (and (not (clientp buffer))
       (not (serverp buffer))))

(defmethod update :around ((buffer buffer))
  (if (solop buffer)
      (call-next-method buffer)
      (when (or (clientp buffer)
		(serverp buffer))
	(when (or (connectedp buffer)
		  (connectingp buffer))
	  (call-next-method)))))

(defmethod update :around ((buffer server-buffer))
  (when (upnp-p) (update-upnp))
  (update-burst-timer)
  (update-echo-timer)
  (cond
    ((timed-out-p)
     (process-messages buffer))
    ((connectingp buffer)
     (process-messages buffer)
     (bursting (try-handshake buffer))
     (bursting (send-frame-state buffer))
     (begin-node-stream (ambient-stream buffer)))
    ((connectedp buffer)
     (bursting (send-flag buffer))
     (querying (try-handshake buffer))
     (process-messages buffer)
     (call-next-method)
     (gracefully (send-frame-state buffer))
     (when (not (streaming))
       (begin-node-stream (background-stream buffer)))
     (send-node-stream)
     (always-bursting
       (make-census buffer)
       (send-message buffer (cons '+state+ (list (flatten-variable '*census*)))))
     (periodically (try-handshake buffer)))))

(defmethod send-frame-state ((buffer server-buffer))
  (send-message buffer (cons '+state+ (flatten-game-variables)))
  (send-message buffer (cons '+state+ (flatten-object-variables))))

(defmethod send-full-state-maybe ((buffer server-buffer) &optional force)
  (when (or force *buffer-dirty-p*)
    (verbosely "Sending full update...")
    (dolist (thing (flatten-game-world))
      (send-message buffer thing))
    (verbosely "Sending full update... Done.")
    (setf *buffer-dirty-p* nil)))

(defun open-server-socket ()
  (setf *server* 
	(usocket:socket-connect nil nil
				:protocol :datagram  
				:timeout 0
				:element-type '(unsigned-byte 8)
				:local-host usocket:*wildcard-host*
				:local-port *server-port*)))

(defun open-client-socket ()
  (setf *server*
	(usocket:socket-connect *server-host* *server-port*
				:protocol :datagram
				:timeout 0
				:element-type '(unsigned-byte 8))))
;;				:local-port *server-port*)))

(defmethod register-socket ((buffer buffer) socket)
  (let ((text (prin1-to-string socket)))
    (logging "Registering socket ~S..."
	     (subseq text 0 (min 60 (length text)))))
  (let ((local-port (usocket:get-local-port socket))
	(local-address (usocket:get-local-address socket)))
    (logging "  local-address:  ~S" local-address)
    (logging "  local-port:     ~S" local-port)))

(defmethod register-socket :after ((buffer server-buffer) socket)
  (logging "Server registering socket local port/host settings...")
  (let ((local-port (usocket:get-local-port socket))
	(local-host (usocket:get-local-address socket)))
    (register-local-host buffer local-host)
    (register-local-port buffer local-port)))

(defmethod register-socket :after ((buffer client-buffer) socket)
  (logging "Client registering socket local port/host settings...")
  (let ((local-port (usocket:get-local-port socket))
	(local-host (usocket:get-local-address socket)))
    (register-local-host buffer local-host)
    (register-local-port buffer local-port)))

(defun start-netplay (&optional (netplay *netplay*))  
  (setf *remote-host* nil)
  (setf *degrade-stream-p* nil)
  (setf *burst-timer* 0)
  (setf *census* nil)
  (setf *node-stream* nil)
  (setf *sent-messages-count* 0)
  (setf *server-sent-messages-count* 0)
  (setf *received-messages-count* 0)
  (logging "Starting netplay...")
  (ecase netplay
    (:server 
     (logging "Starting Xelf server...")
     (logging "Opening server socket...")
     (open-server-socket)
     (logging "Opening server socket... Done.")
     (register-socket (current-buffer) *server*)
     (logging "Starting Xelf server... Done.")
     (logging "Server listening on port ~A " *server-port*))
    (:client
     (setf *terminal-background-color* "dark slate blue")
     (logging "Starting Xelf client..."))))

(defun close-netplay ()
  (when *server* 
    (logging "Closing network socket... ~S" *server*)
    (usocket:socket-close *server*)
    (setf *server* nil)))

