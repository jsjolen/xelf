(in-package :xelf)

;;; Minimal UPnP support for talking to home router.
;;;  see RFC's 6970, 4787, 5405

(defparameter *upnp-udp-port* 1900)
(defparameter *upnp-local-host* nil)
(defvar *upnp-external-ip* nil)
(defvar *upnp-external-ip-requested-p* nil)
(defparameter *upnp-mapping-response* nil)
(defparameter *upnp-mapping-response-string* "u:AddPortMappingResponse")
(defparameter *upnp-gateway-search-string* "urn:schemas-upnp-org:device:InternetGatewayDevice:1")
(defparameter *upnp-service-search-string* "urn:schemas-upnp-org:service:WANIPConnection:1")
(defparameter *upnp-ok-string* "HTTP/1.1 200 OK")
(defparameter *upnp-internal-server-error-string* "HTTP/1.1 500")
(defparameter *upnp-root-device-string* "upnp:rootdevice")
(defparameter *upnp-gateway-device-urn* "urn:schemas-upnp-org:device:InternetGatewayDevice")
(defparameter *upnp-gateway-device-string* "device:InternetGatewayDevice")
(defparameter *upnp-external-ip-prefix* "<NewExternalIPAddress>")
(defparameter *upnp-error-code-prefix* "<errorCode>")
(defparameter *upnp-failure-prefix* "ActionFailed")
(defparameter *upnp-location-prefix* "Location:")
(defparameter *upnp-broadcast-address* "239.255.255.250")
(defvar *upnp-control-url* nil)
(defvar *upnp-control-url-requested-p* nil)

(defvar *upnp-host* nil)
(defvar *upnp-external-ip* nil)
(defvar *upnp-root* nil)
(defvar *upnp-root-port* nil)
(defvar *upnp-location* nil)
(defvar *upnp-mapping-requested-p* nil)

(defvar *upnp-socket* nil)
(defvar *upnp-broadcast-socket* nil)
(defvar *upnp-message* nil)
(defvar *upnp-sent-p* nil)
(defvar *upnp-received-p* nil)
(defvar *upnp-error-code* nil)

(defvar *upnp-listen-host* nil)
(defvar *upnp-listen-port* nil)

(defvar *use-upnp* nil)
(defun upnp-p ()
  *use-upnp*)

(defun reset-upnp ()
  (when *upnp-socket* 
    (usocket:socket-close *upnp-socket*))
  (setf *upnp-socket* nil)
  (when *upnp-broadcast-socket* 
    (usocket:socket-close *upnp-broadcast-socket*))
  (setf *upnp-broadcast-socket* nil)
  (setf *upnp-host* nil)
  (setf *upnp-mapping-response* nil)
  (setf *upnp-location* nil)
  (setf *upnp-listen-host* nil)
  (setf *upnp-local-host* nil)
  (setf *upnp-control-url* nil)
  (setf *upnp-control-url-requested-p* nil)
  (setf *upnp-listen-port* nil)
  (setf *upnp-root* nil)
  (setf *upnp-root-port* nil)
  (setf *upnp-message* nil)
  (setf *upnp-sent-p* nil)
  (setf *upnp-received-p* nil)
  (setf *upnp-external-ip* nil)
  (setf *upnp-external-ip-requested-p* nil)
  (setf *upnp-mapping-requested-p* nil)
  (setf *upnp-error-code* nil))

(defun scrape-string (line tag)
  (let ((p (search tag line)))
    (when (numberp p)
      (subseq line (+ p (length tag))))))

(defun scrape-message (message tag)
  (some #'(lambda (line) 
	    (scrape-string line tag))
	(split-string-on-lines message)))

(defun scrape-ok (message)
  (scrape-message message *upnp-ok-string*))

(defun register-ok (message)
  (when (scrape-ok message)
    (logging "UPnP: Received OK...")))

(defun scrape-root-device (message)
  (scrape-message message *upnp-root-device-string*))
  
(defun scrape-root-ip-and-port (message)
  (let ((location (or (scrape-message message *upnp-location-prefix*)
		      (scrape-message message "LOCATION:"))))
    (when location
      (let* ((digit (position-if #'digit-char-p location))
	     (ip+ (subseq location digit))
	     (colon (when digit (position #\: ip+)))
	     (ip (when colon (subseq ip+ 0 colon)))
	     (port+ (when colon (subseq location (+ colon digit 1))))
	     (slash (when port+ (position #\/ port+)))
	     (port (when slash (subseq port+ 0 slash)))
	     (short-url (when slash (subseq port+ slash))))
	(when (and colon slash)
	  (when port 
	      (let ((port-number (parse-integer port)))
		(when (numberp port-number)
		  (values 
		   (remove-control-characters ip)
		   port-number
		   (remove-control-characters location))))))))))

(defun clean-location-string (string)
  (let ((h (position #\h string))
	(len (length string)))
    (if h
	(subseq string h (- len 1))
	string)))

(defun register-root-device (message &optional force) 
  (let ((root (scrape-root-device message)))
    (when root 
      (when (or force 
	        (and (null *upnp-root*)
		     (not (scrape-message message "Microsoft"))))
		     ;; (scrape-message message *upnp-gateway-device-string*)))
	(multiple-value-bind (ip port location) (scrape-root-ip-and-port message)
	  (setf *upnp-root* ip 
		*upnp-root-port* port
		*upnp-location* location)
	  (logging "UPnP: Registering root device ~A port ~A..." 
		   *upnp-root* *upnp-root-port*)
	  (logging "UPnP: Server location ~A" location))))))

(defun scrape-error-code (message)
  (scrape-message message *upnp-error-code-prefix*))

(defun register-error-code (message)
  (let ((code (scrape-error-code message)))
    (when code 
      (setf *upnp-error-code*
	    (or code *upnp-error-code*))
      (logging "UPnP: Received error ~S" code))))

(defun register-external-ip (message)
  (let ((ip (scrape-external-ip message)))
    (when ip
      (setf *upnp-external-ip* ip)
      (logging "UPnP: Found external IP address: ~A" ip)))) 

(defun crlf ()
  (with-output-to-string (s) 
    (write-char #\return s)
    (write-char #\linefeed s)))

(defun terminate-line (line)
  (concatenate 'string line (crlf)))

(defun terminate-message (message)
  (concatenate 'string message (crlf)))

(defun format-upnp-message (text)
  (terminate-message
   (apply #'concatenate 'string 
	  (mapcar #'terminate-line
		  (split-string-on-lines text)))))

(defun tagify (tag value)
  (format nil "<~A>~A</~A>~%" tag value tag))

(defun upnp-discovery-message (&optional search)
  (format nil "M-SEARCH * HTTP/1.1
Host:239.255.255.250:1900
ST:~A
Man:\"ssdp:discover\"
MX:3" search))

(defun make-discovery-request (&optional (search *upnp-root-device-string*))
  (upnp-discovery-message search))

(defun format-upnp-url (url)
  (if (scrape-string url "://") 
      url
      (format nil "http://~A:~A~A" 
	      *upnp-root*
	      *upnp-root-port*
	      url)))

(defun external-ip-request-header (host length)
  (format nil
"POST ~A HTTP/1.1
Content-Type: text/xml; charset=\"utf-8\" 
SOAPACTION: \"urn:schemas-upnp-org:service:WANIPConnection:1#GetExternalIPAddress\"
Host: ~A
Content-Length: ~A
Connection: Close
Cache-Control: no-cache"
(format-upnp-url *upnp-control-url*)
host length))

(defun external-ip-request-body ()
"<?xml version=\"1.0\"?>
<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" soap:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">
<soap:Body>
<m:GetExternalIPAddress xmlns:m=\"urn:schemas-upnp-org:service:WANIPConnection:1\"/>
</soap:Body>
</soap:Envelope>")

(defun make-external-ip-request (host)
  (let ((body (format-upnp-message (external-ip-request-body))))
    (concatenate 'string
		 (format-upnp-message (external-ip-request-header host (length body)))
		 body)))

(defun port-mapping-request-header (host length)
  (format nil
"POST ~A HTTP/1.1
Content-Type: text/xml; charset=\"utf-8\"
SOAPACTION: \"urn:schemas-upnp-org:service:WANIPConnection:1#AddPortMapping\"
User-Agent: SBCL/1.3 UPnP/1.1 XELF/3.1
Host: ~A:~A
Content-Length: ~S
Connection: Close
Cache-Control: no-cache
"
*upnp-control-url*
host *upnp-root-port* length))

(defun port-mapping-request-body 
    (&key remote-host external-port protocol 
	  internal-port internal-client 
	  enabled-p duration description)
  (concatenate 'string
"<?xml version=\"1.0\"?>
<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
<soap:Body>
<m:AddPortMapping xmlns:m=\"urn:schemas-upnp-org:service:WANIPConnection:1\">
"
(tagify "NewRemoteHost" remote-host)
(tagify "NewExternalPort" external-port)
(tagify "NewProtocol" protocol)
(tagify "NewInternalPort" internal-port)
(tagify "NewInternalClient" internal-client)
(tagify "NewEnabled" enabled-p)
(tagify "NewLeaseDuration" duration)
(tagify "NewPortMappingDescription" description)
"</m:AddPortMapping>
</soap:Body>
</soap:Envelope>"))

(defun make-port-mapping-request (host args)
  (let ((body (format-upnp-message (apply #'port-mapping-request-body args))))
     (concatenate 'string
		  (format-upnp-message 
		   (port-mapping-request-header host (length body)))
		  body)))

(defun describe-socket (socket &optional remotep)
  (let ((text (prin1-to-string socket)))
    (logging "Inspecting socket ~S..."
	     (subseq text 0 (min 60 (length text)))))
  (let ((local-port (usocket:get-local-port socket))
	(local-address (usocket:get-local-address socket)))
    (logging "  local-address:   ~S" local-address)
    (logging "  local-port:      ~S" local-port))
  (when remotep
    (let ((peer-port (usocket:get-peer-port socket))
	  (peer-address (usocket:get-peer-address socket)))
      (logging "  peer-address:   ~S" peer-address)
      (logging "  peer-port:      ~S" peer-port)))
  socket)

(defun open-upnp-broadcast ()
  (setf *upnp-broadcast-socket*
	(usocket:socket-connect nil nil
				:protocol :datagram
				:timeout 0
				:element-type '(unsigned-byte 8)
				:local-host usocket:*wildcard-host*))
  (setf (usocket:socket-option *upnp-broadcast-socket* :broadcast) t))

(defun send-upnp-broadcast (message)
  (usocket:socket-send *upnp-broadcast-socket*
		       message 
		       (length message) 
		       :port *upnp-udp-port*
		       :host *upnp-broadcast-address*))

(defun open-upnp-socket ()
  (setf *upnp-socket*
	(usocket:socket-connect *upnp-root* *upnp-root-port* 
				:protocol :stream
				:timeout 5
				:element-type 'character))
  (setf *upnp-local-host* 
	(expand-ip-vector
	 (usocket:get-local-address *upnp-socket*))))

;;				:local-port *upnp-root-port*)))

(defun upnp-message-ready-p (socket)
  (when socket
    (usocket:wait-for-input socket :timeout 0 :ready-only t)))

(defun find-upnp-host () 
  (or *upnp-host* *upnp-broadcast-address*))

(defun send-upnp-message (message &optional (host (find-upnp-host)) (socket *upnp-socket*) (port *upnp-udp-port*))
  (let ((message2 (format-upnp-message message)))
    (usocket:socket-send socket message2 (length message2) :port port :host host)))

(defun send-upnp-request (message)
  (let ((stream (usocket:socket-stream *upnp-socket*)))
    (write-sequence message stream)
    (write-sequence message *standard-output*)
    (force-output stream)))
  
(defun send-discovery-request ()
  (send-upnp-message 
   (make-discovery-request)
   *upnp-broadcast-address*
   *upnp-broadcast-socket*))

(defun make-xml-request (url)
  (format-upnp-message
   (format nil "GET ~A HTTP/1.1
User-Agent: SBCL/1.3 UPnP/1.1 XELF/3.1
" url)))

(defun send-xml-request ()
  (send-upnp-request
   (make-xml-request *upnp-location*)))

(defun send-external-ip-request ()
  (send-upnp-request 
   (make-external-ip-request *upnp-root*)))

(defun expand-ip-vector (vector)
  (let ((octets (coerce vector 'list)))
    (destructuring-bind (a b c d) octets
      (format nil "~S.~S.~S.~S" a b c d))))

(defun record-upnp-message (message host port)
  (setf *upnp-host* (etypecase host
		      (string host)
		      (vector (expand-ip-vector host))))
  (setf *upnp-listen-port* port)
  (setf *upnp-message* message))

(defun scrape-control-url (message)
  (or (scrape-string message "<controlURL>")
      (scrape-string message "<controlurl>")
      (scrape-string message "<CONTROLURL>")
      (scrape-string message "<ControlUrl>")))

(defun register-xml (message)
  (let ((field (scrape-string message *upnp-service-search-string*)))
    (when field
      (logging "UPnP: Received XML with WANIPConnection service ID.")
      (when (not *upnp-external-ip-requested-p*)
	(if (scrape-message message *upnp-gateway-device-urn*)
	    (logging "UPnP: Found Gateway device URN.")
	    (logging "UPnP: Warning: Did not find Gateway device URN."))
	(when *upnp-root*
	  (if (scrape-message message *upnp-root*)
	      (logging "UPnP: Found existing root device IP here.")
	      (logging "UPnP: Warning: Did not find existing root device IP."))))
      (let ((url-begin (scrape-control-url field)))
	(when url-begin
	  (logging "Found URL begin tag.")
	  (let ((end (position-if (lambda (c) (find c #(#\< #\Return #\Newline)))
				  url-begin)))
	    (when end
	      (setf *upnp-control-url* 
		    (subseq url-begin 0 end))
	      (logging "Extracted ControlURL: ~A" *upnp-control-url*))))))))

(defun register-mapping-response (message)
  (when (and (scrape-message message "HTTP")
	     (scrape-message message "Error"))
    (logging "UPnP: Received HTTP Error. Text sent to stdout."))
  (when (and (scrape-message message *upnp-mapping-response-string*)
	     (scrape-message message *upnp-ok-string*))
    (logging "UPnP: Received <AddPortMappingResponse> OK")
    (logging "UPnP FINISHED. PRESS [ENTER] TO BEGIN ONLINE PLAY.")))

(defun register-external-ip-response (message)
    (when (scrape-message message *upnp-ok-string*)
      (let ((ip+ (scrape-string message *upnp-external-ip-prefix*)))
	(when ip+
	  (logging "UPnP: Found <NewExternalIpAddress> tag.")
	  (let ((end (position-if (lambda (c) (find c #(#\< #\Return #\Newline)))
				  ip+)))
	    (when end
	      (logging "UPnP: Found External IP response: ~S"
		       (setf *upnp-external-ip* (subseq ip+ 0 end)))))))))

(defun process-upnp-message (message)
  (register-xml message)
  (register-root-device message)
  (register-error-code message)
  (register-ok message)
  (when *upnp-mapping-requested-p*
    (register-mapping-response message))
  (when *upnp-external-ip-requested-p*
    (register-external-ip-response message)))

(defun process-upnp-messages (socket)
  (loop while (upnp-message-ready-p socket) do
    (multiple-value-bind (octets length host port)
	(usocket:socket-receive socket *datagram* *datagram-length*)
      (record-upnp-message (babel:octets-to-string 
			    (subseq *datagram* 0 length))
			   host port)
      (process-upnp-message *upnp-message*))))

(defun snarf (stream)
  (with-output-to-string (s)
    (block reading
      (let (line)
	(loop while (setf line (read-line stream nil :eof))
	      do (if (eq :eof line)
		     (return-from reading)
		     (write-line line s)))))))

(defun process-upnp-stream (socket)
  (when (usocket:wait-for-input socket :timeout 0 :ready-only t)
    (let ((stream (usocket:socket-stream socket)))
      (let ((message (snarf stream)))
	(when (plusp (length message))
	  (record-upnp-message message *upnp-host* 0)
	  (process-upnp-message message))))))

(defparameter *port-mapping-duration* 0)

(defun port-mapping-arguments ()
  (list :remote-host ""
	:external-port *server-port*
	:internal-port *server-port*
	:internal-client *upnp-local-host*
	:protocol "UDP"
	:enabled-p 1
	:description "xelf:nat:upnp"
	:duration *port-mapping-duration*))

(defun send-port-mapping-request ()
  (let ((request (make-port-mapping-request
		  *upnp-local-host*
		  (port-mapping-arguments))))
    (prin1 request)
    (send-upnp-request request)))

(defun initialize-upnp ()
  (logging "UPnP: Initializing...")
  (reset-upnp)
  (logging "UPnP: Opening broadcast socket...")
  (open-upnp-broadcast)
  (describe-socket *upnp-broadcast-socket*)
  (logging "UPnP: Sending discovery request.")
  (send-discovery-request)
  (send-discovery-request))

(defun update-upnp ()
  (process-upnp-messages *upnp-broadcast-socket*)
  (when *upnp-socket* 
    (process-upnp-stream *upnp-socket*))
  (when (and *upnp-host* 
	     *upnp-location*
	     *upnp-socket*
	     (null *upnp-control-url-requested-p*)
	     (null *upnp-control-url*))
    (logging "UPnP: Requesting XML...")
    (send-xml-request)
    (setf *upnp-control-url-requested-p* t))
  (when (and *upnp-host*
	     (not *upnp-control-url-requested-p*)
	     (not *upnp-control-url*)
	     (not *upnp-mapping-requested-p*))
    (logging "UPnP: Opening gateway socket...")
    (when *upnp-socket* (usocket:socket-close *upnp-socket*))
    (open-upnp-socket)
    (describe-socket *upnp-socket* t))
  (when (and *upnp-host*
	     (not *upnp-external-ip-requested-p*)
	     *upnp-control-url*)
    (when *upnp-socket* (usocket:socket-close *upnp-socket*))
    (open-upnp-socket)
    (describe-socket *upnp-socket* t)
    (logging "UPnP: Sending external IP request...")
    (send-external-ip-request)
    (setf *upnp-external-ip-requested-p* t))
  (when (and *upnp-host*
	     (not *upnp-mapping-requested-p*)
	     *upnp-external-ip*)
    (when *upnp-socket* (usocket:socket-close *upnp-socket*))
    (open-upnp-socket)
    (describe-socket *upnp-socket* t)
    (logging "UPnP: Sending port map request...")
    (send-port-mapping-request)
    (setf *upnp-mapping-requested-p* t)))

